<?php

namespace Core;

class Core
{
	public function run()
	{
		include 'src/routes.php';
		$BASE = BASE_URI;
		$SERVER= $_SERVER['REQUEST_URI'];
		$URI = str_replace($BASE, "", $SERVER);
		$router = new Router();
		$get = $router->get($URI);
		$URI = explode('/', $URI);
		switch($get['controller'])
		{
			case 'app':
			if($get['action'] == 'index')
			{
				$AppController = new \Controller\AppController();
				$AppController->indexAction(); 
			}
			break;
			case 'user':
			if ($get['action'] =='add')
			{
				if (empty($_POST['email']) || empty($_POST['pass']))
				{
					$UserController = new \Controller\UserController();
					$UserController->addAction();
				}
				else
				{
					$UserController = new \Controller\UserController();
					$UserController->registerAction($_POST);
				}
			}	
			elseif ($get['action'] =='login') 
			{
				if (empty($_POST['email']) || empty($_POST['pass']))
				{
					$UserController = new \Controller\UserController();
					$UserController->connexionView();
				}
				else
				{
					$UserController = new \Controller\UserController();
					$UserController->connexion($_POST);
				}
			}
			elseif ($get['action'] == 'show')
			{
				$UserController = new \Controller\UserController();
				$UserController->showAction();
			}
			elseif ($get['action'] == 'account') 
			{
				if (isset($_POST['modif']))
				{
					$UserController = new \Controller\UserController();
					$UserController->showAccount();
				}
				else
				{
					$UserController = new \Controller\UserController();
					$UserController->updateAccount($_POST);
				}
			}
			elseif ($get['action'] == 'deconnexion')
			{
				
				$UserController = new \Controller\UserController();
				$UserController->deconnexion();
			}
			elseif ($get['action'] == 'delete')
			{
				if (empty($_POST))
				{
					$UserController = new \Controller\UserController();
					$UserController->deleteView();
				}
				elseif ($_POST['delete'] =='yes')
				{
					$UserController = new \Controller\UserController();
					$UserController->deleteAction($_POST);
				}
				else
				{
					header('Location: show');
				}
			}
			break;
			case 'film':
			if($get['action'] == 'viewMovie')
			{	
				if(isset($_POST['delete']))
				{
					$FilmController = new \Controller\FilmController();
					$FilmController->deleteFilm();
				}
				else
				{
					$FilmController = new \Controller\FilmController();
					$FilmController->viewMovie();
				}
			}
			elseif ($get['action'] == 'addMovie')
			{
				if (isset($_POST["addMovie"]) && !empty($_POST["titre"]) && !empty($_POST["resum"]) && !empty($_POST["id_genre"])) {
					$FilmController = new \Controller\FilmController();
					$FilmController->addMovie(); 
				}
				else{
					$FilmController = new \Controller\FilmController();
					$FilmController->addMovieView(); 
				}
			}
			elseif ($get['action'] == 'updateFilm')
			{
				if(isset($_POST['update']))
				{
					$FilmController = new \Controller\FilmController();
					$FilmController->updateFilm();
				}
				else
				{
					$FilmController = new \Controller\FilmController();
					$FilmController->viewUpdate();
				}
			}

			break;
			case '404':
			echo '404';
			break;
		}
	}	
}
?>
