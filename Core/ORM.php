<?php

namespace Core;

class ORM
{
	public function __construct()
	{
		try
		{
			$this->_bdd = new \PDO('mysql:host=localhost;dbname=epitech_tp;charset=utf8', 'root', '');
		}
		catch (\Exception $e)
		{
			die('Erreur : ' . $e->getMessage());
		}
	}
	public function create($table, $fields)
	{
		$fields_keys = array_keys($fields);
		$tab = [];
		$arrayFields = [];
		foreach ($fields as $key => $value) 
		{
			$tab[] = '?';
			$arrayFields[] = $value;
		}

		$columns = implode(",", $fields_keys);
		$unknown = implode(",", $tab);
		$request = $this->_bdd->prepare("INSERT INTO $table($columns) VALUES($unknown)");
		$request->execute($arrayFields);
		return $this->_bdd->lastInsertId();
	}
	public function read($table, $id)
	{
		$request = $this->_bdd->prepare("SELECT * FROM $table WHERE id = ?");
		$request->execute(array($id));
		return $request->fetchAll();
	}	
	public function update($table, $fields, $id, $id_name = "id_perso")
	{
		$array = [];
		$allValues = [];
		foreach ($fields as $key => $value) 
		{
			$array[] = "$key = ?";
			$allValues[] = $value;
		}
		$allValues[] = $id;
		$set_values = implode(",", $array);
		$request = $this->_bdd->prepare("UPDATE $table SET $set_values WHERE $id_name = ?");
		$request->execute($allValues);
	}
	public function delete($table, $id, $id_name = "id_perso")
	{
		$req = $this->_bdd->prepare("DELETE FROM $table WHERE $id_name = ?");
		return $req->execute(array($id));
	}
	public function find($table, $params = array('WHERE' => '1'))
	{
		$tab = [];
		foreach ($params as $key => $value) 
		{
			$tab []= $key." ".$value;
		}
		$request = implode($tab);
		$req = $this->_bdd->query("SELECT * FROM $table $request");
		return $req->fetchAll(\PDO::FETCH_ASSOC);
	}
}
?>
