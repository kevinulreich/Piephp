<?php

namespace Core;

class Request
{
	public $request;
	
	public function __construct()
	{
		foreach ($_POST as $key => $value) {
			$this->request[$key]= htmlspecialchars(trim($value));
		}
		foreach ($_GET as $key => $value) {
			$this->request[$key]= htmlspecialchars(trim($value));
		}
	}
	public function getQueryParams()
	{
		return $this->request;
	}	
}
?>
