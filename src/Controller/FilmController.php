<?php
namespace Controller;

session_start();

class FilmController extends \Core\Controller
{
	private $request;

	public function __construct()
	{
		$this->request = new \Core\Request();
	}
	public function viewMovie()
	{
		$params = $this->request->getQueryParams();
		$FilmModel = new \Model\FilmModel($params);
		$result = $FilmModel->viewMovie();
		echo $this->render('viewMovie', ["css" => "webroot/css/style.css", "title" => "Film", "film"=>$result]);
	}
	public function addMovieView()
	{
		$FilmModel = new \Model\FilmModel([]);
		$result = $FilmModel->getGenre();
		echo $this->render('addMovie', ["css" => "webroot/css/style.css", "title" => "Ajout de film", "genre" => $result]);
	}
	public function addMovie()
	{
		$params = $this->request->getQueryParams();
		$FilmModel = new \Model\FilmModel($params);
		$result = $FilmModel->AddMovie();
	}
	public function viewUpdate()
	{
		echo $this->render('updateFilm', ["css" => "webroot/css/style.css", "title" => "Modification de film"]);
	}
	public function updateFilm()
	{
		$params = $this->request->getQueryParams();
		$FilmModel = new \Model\FilmModel($params);
		$result = $FilmModel->updateFilm();
	}
	public function deleteFilm()
	{
		$params = $this->request->getQueryParams();
		$FilmModel = new \Model\FilmModel($params);
		$FilmModel->deleteFilm();
	}

}