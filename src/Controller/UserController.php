<?php
namespace Controller;

session_start();

class UserController extends \Core\Controller
{
	private $request;

	public function __construct()
	{
		$this->request = new \Core\Request();
	}
	public function addAction()
	{		
		echo $this->render('register', ["css" => "webroot/css/style.css", "title" => "Inscription"]);
	}
	public function registerAction($post)
	{
		$params = $this->request->getQueryParams();
		$UserModel = new \Model\UserModel($params);
		$UserModel->save();
		header('Location:login');
	}
	public function connexionView()
	{
		echo $this->render('login', ["css" => "webroot/css/style.css", "title" => "Login"]);
	}
	public function connexion()
	{
		$params = $this->request->getQueryParams();
		$UserModel = new \Model\UserModel($params);
		$info_co = $UserModel->login();
		if ($info_co == true) 
		{
			header('Location: show'); 
		}
	}
	public function showAction()
	{
		$UserModel = new \Model\UserModel(['id'=>$_SESSION['id_perso']]);
		$results = $UserModel->showMovies();
		echo $this->render('show', ["css" => "webroot/css/style.css", "title" => "Home", "all_movies"=>$results]);
	}

	public function showAccount()
	{	
		echo $this->render('account', ["css" => "webroot/css/style.css", "title" => "Modification"]);
	}
	public function updateAccount()
	{
		$params = $this->request->getQueryParams();
		$UserModel = new \Model\UserModel($params);
		$UserModel->update();
	}
	public function deconnexion()
	{
		session_destroy();
		header('Location: login');
	}
	public function deleteView()
	{
		echo $this->render('delete', ["css" => "webroot/css/style.css", "title" => "Supprimer compte"]);
	}
	public function deleteAction()
	{
		$params = $this->request->getQueryParams();
		$UserModel = new \Model\UserModel($params);
		$UserModel->delete();
	}
}
?>