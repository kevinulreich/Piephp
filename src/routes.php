<?php
Core\Router::connect('/', ['controller' => 'app', 'action' => 'index']);
Core\Router::connect('/register', ['controller' => 'user', 'action' => 'add']);
Core\Router::connect('/login', ['controller' => 'user', 'action' => 'login']);
Core\Router::connect('/show', ['controller' => 'user', 'action' => 'show']);
Core\Router::connect('/account', ['controller' => 'user', 'action' => 'account']);
Core\Router::connect('/deconnexion', ['controller' => 'user', 'action' => 'deconnexion']);
Core\Router::connect('/delete', ['controller' => 'user', 'action' => 'delete']);
Core\Router::connect('/viewMovie', ['controller' => 'film', 'action' => 'viewMovie']);
Core\Router::connect('/addMovie', ['controller' => 'film', 'action' => 'addMovie']);
Core\Router::connect('/updateFilm', ['controller' => 'film', 'action' => 'updateFilm']);
?>