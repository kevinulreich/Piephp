<?php

namespace Model;

class FilmModel extends \Core\Entity
{
	private $_bdd;
	private $_email;
	private $_password;

	public function __construct($params)
	{
		parent::__construct($params);
	}
	public function viewMovie()
	{
		$orm = new \Core\ORM();
		return $orm->find("film", ["WHERE"=>"id_film = '$this->id_film'"]);
	}
	public function addMovie()
	{
		$orm = new \Core\ORM();
		$orm->create("film", ["titre"=>$this->titre,"resum"=>$this->resum, "id_genre" => $this->id_genre]);
		header('Location: show');
	}
	public function getGenre()
	{
		$orm = new \Core\ORM();
		return $orm->find("genre");
	}
	public function updateFilm()
	{
		$orm = new \Core\ORM();
		$info_user = $orm->update("film", ["titre"=> $this->titre, "resum" => $this->resum], $this->id, "id_film");
		header('Location: show');
	}
	public function deleteFilm()
	{
		$orm = new \Core\ORM();
		$orm->delete("film", $this->id, "id_film");
		header('Location: show');
	}
}
