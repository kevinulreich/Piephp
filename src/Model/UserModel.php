<?php
namespace Model;

class userModel extends \Core\Entity
{
	private $_bdd;
	private $_email;
	private $_password;

	public function __construct($params)
	{
		parent::__construct($params);
		try
		{
			$this->_bdd = new \PDO('mysql:host=localhost;dbname=epitech_tp;charset=utf8', 'root', '');
		}
		catch (\Exception $e)
		{
			die('Erreur : ' . $e->getMessage());
		}
	}
	public function save()
	{
		$orm = new \Core\ORM();
		$pass = password_hash($this->pass, PASSWORD_DEFAULT);
		$orm->create("fiche_personne", ["email"=>$this->email,"password"=>$pass]);
	}
	public function login()
	{	
		$orm = new \Core\ORM();
		$info_user = $orm->find("fiche_personne", ["WHERE"=>"email = '$this->email'"]);
		if (empty($info_user))
		{
			echo "Pas de mail <br/>";
			return false;
		}
		elseif (!password_verify($this->pass, $info_user[0]["password"])) 
		{
			echo "Mauvais mot de passe<br/>";
			return false;
		}
		else
		{
			$_SESSION["password"] = $this->pass;
			$_SESSION["email"] = $this->email;
			$_SESSION["id_perso"] = $info_user[0]['id_perso'];		
			return true;
		}
	}
	public function read($id)
	{
		$req = $this->_bdd->prepare("SELECT * FROM users WHERE id = ?");
		$req->execute(array($id));
		$rep = $req->fetch();
	}
	public function update()
	{
		$orm = new \Core\ORM();
		$info_user = $orm->update("fiche_personne", ["email"=> $this->email], $_SESSION["id_perso"]);
		$idperso = $_SESSION["id_perso"] ;
		session_destroy();
		session_start();
		$_SESSION["password"] = $this->pass;
		$_SESSION["email"] = $this->email;
		$_SESSION["id_perso"] = $idperso;
		header('Location: show');
	}
	public function deconnexion()
	{
		session_destroy();
		header('Location: login');
	}
	public function delete()
	{
		$orm = new \Core\ORM();
		$info_user = $orm->delete("fiche_personne", $_SESSION["id_perso"]);
		header('Location: register');
	}
	public function showMovies()
	{
		$orm = new \Core\ORM();		
		return $orm->find("film", [" ORDER BY " => " titre "]);
	}
}

?>